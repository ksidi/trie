package ksidis.trees.trie;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.io.File;
import java.io.FileNotFoundException;

import java.net.URL;
import java.util.*;

/**
 * Unit test for simple App.
 */
public class TrieTest extends TestCase {
    private  File inputFile = null;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TrieTest(String testName)
    {
        super( testName );
    }


    public void setUp() throws Exception {
        super.setUp();
        URL url = this.getClass().getResource("words_alpha.txt");
        Assert.assertNotNull(url);
        inputFile = new File(url.toURI());
        Assert.assertNotNull(inputFile);
    }

    /**
     * Simple Test
     */
    public void testTrie() {
        System.out.println(" >> Testing trie...");
        List<String> words = Arrays.asList("car", "carbon", "carriage", "cartoon", "butane", "propane", "arlington", "arlington");
        Trie<Integer> trie = new Trie<>();

        words.forEach(word -> {
            System.out.println(" >> Adding ... " + word);
            trie.put(word, 1);
        });

        System.out.println(" >> Trie size: " + trie.size());

        System.out.println(" >> Trie contains \"cartoon\"  -> [" + trie.contains("cartoon") + "]");
        System.out.println(" >> Trie contains \"bobby\"  -> [" + trie.contains("bobby") + "]");
        System.out.println(" >> Trie contains \"cotton\"  -> [" + trie.contains("cotton") + "]");
        Integer value = trie.get("cartoon");
        assertNotNull(value);
        assertEquals(value.intValue(), 1);

        ArrayList<String> predictions = trie.predict("car");
        System.out.println(" >> predictions for \"car\" ..." + predictions);
    }

    /**
     * Measure the performance of Trie and compare it to brute force approach
     */
    public void testPerformance() throws FileNotFoundException {
        int repetitions = 10000;
        long start = System.nanoTime(); //warm up
        long totalElapsedBrute = 0;
        long totalElapsedTrieA = 0;

        //Read the file and create an Array list of strings
        ArrayList<String> words = new ArrayList<>();

        Scanner s = new Scanner(inputFile);
        while (s.hasNextLine()){
            words.add(s.nextLine().trim());
        }
        s.close();

        Trie<Integer> trie = new Trie<>();
        for (String w : words) {
            trie.put(w, 1);
        }
        System.out.println();
        System.out.println(" >> Testing performance ...");
        System.out.println(" >> " + words.size() + " words read...");

        for (int i = 1000; i < 1000 + repetitions; i++) {
            int index = (int) (Math.random() * words.size());
            String aWord = words.get(index);
            //System.out.println(index + " - " + aWord);

            boolean isThere;
            start = System.nanoTime();
            isThere = trie.contains(aWord);
            totalElapsedTrieA += (System.nanoTime() - start);
            if (!isThere) System.out.print(".");

            start = System.nanoTime();
            isThere = words.contains(aWord);
            totalElapsedBrute += (System.nanoTime() - start);
            if (!isThere) System.out.print(".");
        }

        System.out.println(" >> Brute force for " + repetitions + " words: " + totalElapsedBrute/10e9);
        System.out.println(" >> New Trie for " + repetitions + " words: " + totalElapsedTrieA/10e9);
    }
}
