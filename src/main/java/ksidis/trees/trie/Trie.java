package ksidis.trees.trie;

import java.util.ArrayList;

/**
 * A simple implementation of a trie.
 * It supports only 255 ASCII characters.
 *
 * @author Kostas Sidiropoulos <ksidis@gmail.com>
 */
public class Trie<Value> {
    private static final int R = 255;

    private TrieNode root;
    private int size = 0;

    private static class TrieNode {
        private char c;
        private TrieNode[] children = new TrieNode[R];
        private Object value;
    }

    /**
     * Checks whether a key is already in the trie
     *
     * @param key the key to check
     * @return true if the key is present in the Trie
     */
    public boolean contains(String key) {
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("Key cannot be null or empty");
        return (get(key) != null);
    }

    /**
     * Retrieves the value for a given key
     *
     * @param key the key to get
     * @return the value associated with this key
     */
    public Value get(String key) {
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("Key cannot be null or empty");
        TrieNode x = get(root, key, 0);
        if (x == null) return null;
        return (Value) x.value;
    }

    private TrieNode get(TrieNode x, String key, int d) {
        if (x == null) return null;
        if (d == key.length()) return x;
        char c = key.charAt(d);
        return get(x.children[c], key, d+1);
    }

    /**
     * Checks whether the Trie is empty
     *
     * @return true if Trie is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Inserts a key into the Trie
     *
     * @param key the key to insert
     * @param value the value to associate with this key
     */
    public void put(String key, Value value) {
        if (key == null || key.isEmpty() || value == null) throw new IllegalArgumentException("Key and value cannot be null");
        root = put(root, key, value, 0);
    }

    private TrieNode put(TrieNode x, String key, Value value, int d) {
        if (x == null) {
            x = new TrieNode();
        }

        if (d == key.length()) {
            if (x.value == null) {
                size++;
            }
            x.value = value;
            return x;
        }
        char c = key.charAt(d);
        x.children[c] = put(x.children[c], key, value, d+1);
        return x;
    }

    /**
     * Returns a list of keys matching the given prefix
     *
     * @param prefix the prefix to start
     * @return a list of the the keys that start with the given prefix
     */
    public ArrayList<String> predict(String prefix) {
        ArrayList<String> results = new ArrayList();
        TrieNode x = get(root, prefix, 0);
        predict(x, prefix, results);
        return results;
    }

    private void predict(TrieNode x, String prefix, ArrayList<String> results) {
        if (x == null) return;
        if (x.value != null)
            results.add(prefix);
        for (char c = 0; c < R; c++) {
            predict(x.children[c], prefix + c, results);
        }
    }

    /**
     * Returns the number of keys present in the Trie.
     *
     * @return the size of the Trie
     */
    public int size() {
        return size;
    }

    /**
     * Removes a key for the Trie
     *
     *
     * @param key the key to remove
     */
    public void delete(String key) {
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("Key cannot be null or empty");
        root = delete(root, key, 0);
    }

    private TrieNode delete(TrieNode x, String key, int d) {
        if (x == null) return null;
        if (d == key.length()) {
            if (x.value != null) size--;
            x.value = null;
        }
        else {
            char c = key.charAt(d);
            x.children[c] = delete(x.children[c], key, d+1);
        }

        // remove subtrie rooted at x if it is completely empty
        if (x.value != null) return x;
        for (int c = 0; c < R; c++)
            if (x.children[c] != null)
                return x;
        return null;
    }
}
