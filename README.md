## What is this?

This is a basic JAVA implementation of Trie.

## How to use this library?

Simply add the following dependency


```html
<dependencies>
    ...
    <dependency>
         <groupId>ksidis.trees</groupId>
         <artifactId>Trie</artifactId>
         <version>1.0-SNAPSHOT</version>
    </dependency>
<dependencies>
```
### Examples

Instantiate a new Trie

```java
Trie<Integer> trie = new Trie<>();
```

Add new keys using the put method

```java
trie.put("car", 1);
trie.put("carriage", 2);
trie.put("carbon", 3);
```
get predictions based on a given prefix:

```java
ArrayList<String> predictions = trie.predict("car");
```